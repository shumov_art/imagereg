﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using System.Threading;

namespace ImageReg
{
    class Program
    {
        static GrayscaleFloatImage NewImage(GrayscaleFloatImage image, int n)
        {
            GrayscaleFloatImage image2 = new GrayscaleFloatImage(image.Width + 2 * n, image.Height + 2 * n);
            for (int y = n; y < image.Height + n; y++)
            {
                for (int x = n; x < image.Width + n; x++)
                {
                    image2[x, y] = image[x - n, y - n];
                }
            }
            for (int y = 0; y < n; y++)
            {
                for (int x = 0; x < n; x++)
                {
                    image2[x, y] = image[x, y];
                    image2[image2.Width - x - 1, y] = image[image.Width - x - 1, y];
                    image2[x, image2.Height - y - 1] = image[x, image.Height - y - 1];
                    image2[image2.Width - x - 1, image2.Height - y - 1] = image[image.Width - x - 1, image.Height - y - 1];
                }
            }
            for (int y = 0; y < n; y++)
            {
                for (int x = n; x < image2.Width - n; x++)
                {
                    image2[x, y] = image[x - n, y];
                    image2[x, image2.Height - y - 1] = image[x - n, image.Height - y - 1];
                }
            }
            for (int y = n; y < image2.Height - n; y++)
            {
                for (int x = 0; x < n; x++)
                {
                    image2[x, y] = image[x, y - n];
                    image2[image2.Width - x - 1, y] = image[image.Width - x - 1, y - n];
                }
            }
            return image2;
        }

        static double MSE(GrayscaleFloatImage image, GrayscaleFloatImage image2)
        {
            double m = 0, r = 0;
            for (int y = 10; y < image.Height - 11; y++)
            {
                for (int x = 10; x < image.Width - 11; x++)
                {
                    r = image[x, y] - image2[x, y];
                    m += r * r;
                }
            }
            return m / (image.Height * image.Width);
        }

        static GrayscaleFloatImage Gauss(GrayscaleFloatImage image, int n, double[] mask)
        {
            GrayscaleFloatImage GaussedImage_interim = new GrayscaleFloatImage(image.Width, image.Height),
                                GaussedImage_fin = new GrayscaleFloatImage(image.Width, image.Height);
            GrayscaleFloatImage image2 = NewImage(image, n);
            double sum1 = 0;
            for (int y = n; y < image.Height + n; y++)
            {
                for (int x = n; x < image.Width + n; x++)
                {
                    sum1 = 0;
                    for (int i = -n; i < n + 1; i++)
                    {
                        sum1 = sum1 + mask[i + n] * image2[x + i, y];
                    }
                    GaussedImage_interim[x - n, y - n] = (float)sum1;
                }
            }
            GrayscaleFloatImage GaussedImage_interim2 = NewImage(GaussedImage_interim, n);
            for (int x = n; x < image.Width + n; x++)
            {
                for (int y = n; y < image.Height + n; y++)
                {
                    sum1 = 0;
                    for (int i = -n; i < n + 1; i++)
                    {
                        sum1 = sum1 + mask[i + n] * GaussedImage_interim2[x, y + i];
                    }
                    GaussedImage_fin[x - n, y - n] = (float)sum1;
                }
            }
            return GaussedImage_fin;
        }

        static void Building_Gauss_mask(int n_gauss, double s_gauss, out double[] mask_gauss)
        {
            mask_gauss = new double[2 * n_gauss + 1];
            double sum = 0;
            for (int x = 0; x < n_gauss + 1; x++)
            {
                if (x == 0) sum = sum + 1;
                if (x != 0) sum = sum + 2 * Math.Exp((x * x) / -s_gauss);
                mask_gauss[x + n_gauss] = Math.Exp((x * x) / -s_gauss);
                mask_gauss[n_gauss - x] = Math.Exp((x * x) / -s_gauss);
            }
            for (int x = 0; x < 2 * n_gauss + 1; x++)
            {
                mask_gauss[x] = mask_gauss[x] / sum;
            }
        }

        static void Building_Gradient_mask(int n_grad, double s_grad, out double[,] mask_grad_x, out double[,] mask_grad_y)
        {

            mask_grad_x = new double[2 * n_grad + 1, 2 * n_grad + 1];
            mask_grad_y = new double[2 * n_grad + 1, 2 * n_grad + 1];
            for (int y = 0; y < n_grad + 1; y++)
            {
                for (int x = 0; x < n_grad + 1; x++)
                {
                    mask_grad_x[x + n_grad, y + n_grad] = 2 * x * Math.Exp((x * x + y * y) / -s_grad) / s_grad;
                    if (x != 0) mask_grad_x[n_grad - x, y + n_grad] = -2 * x * Math.Exp((x * x + y * y) / -s_grad) / s_grad;
                    if (y != 0) mask_grad_x[x + n_grad, n_grad - y] = 2 * x * Math.Exp((x * x + y * y) / -s_grad) / s_grad;
                    if (x != 0 & y != 0) mask_grad_x[n_grad - x, n_grad - y] = -2 * x * Math.Exp((x * x + y * y) / -s_grad) / s_grad;
                }
            }
            for (int y = 0; y < n_grad + 1; y++)
            {
                for (int x = 0; x < n_grad + 1; x++)
                {
                    mask_grad_y[x + n_grad, y + n_grad] = 2 * y * Math.Exp((x * x + y * y) / -s_grad) / s_grad;
                    if (x != 0) mask_grad_y[n_grad - x, y + n_grad] = 2 * y * Math.Exp((x * x + y * y) / -s_grad) / s_grad;
                    if (y != 0) mask_grad_y[x + n_grad, n_grad - y] = -2 * y * Math.Exp((x * x + y * y) / -s_grad) / s_grad;
                    if (x != 0 & y != 0) mask_grad_y[n_grad - x, n_grad - y] = -2 * y * Math.Exp((x * x + y * y) / -s_grad) / s_grad;
                }
            }
        }

        static void Building_VectorField_du_W_symm(GrayscaleFloatImage Image1, GrayscaleFloatImage Image_Modified, GrayscaleFloatImage Image1_GradMod,
                                              GrayscaleFloatImage Image1_GradValue_x, GrayscaleFloatImage Image1_GradValue_y, GrayscaleFloatImage Image2_GradMod,
                                              GrayscaleFloatImage Image2_GradValue_x, GrayscaleFloatImage Image2_GradValue_y,
                                              out GrayscaleFloatImage VectorField_du_x, out GrayscaleFloatImage VectorField_du_y)
        {
            VectorField_du_x = new GrayscaleFloatImage(Image1.Width, Image1.Height);
            VectorField_du_y = new GrayscaleFloatImage(Image1.Width, Image1.Height);
            float a11 = 0, a12_21 = 0, a22 = 0, b1 = 0, b2 = 0, det = 0, w = 0;

            for (int y = 0; y < Image1.Height; y++)
            {
                for (int x = 0; x < Image1.Width; x++)
                {
                    w = 0; a11 = 0; a12_21 = 0; a22 = 0; b1 = 0; b2 = 0; det = 0;
                    for (int t = -2; t < 3; t++)
                    {
                        for (int l = -2; l < 3; l++)
                        {
                            if (x + t >= 0 && x + t < Image1.Width && y + l >= 0 && y + l < Image1.Height)
                            {
                                if (Math.Abs((Image1_GradValue_x[x + t, y + l] * Image2_GradValue_x[x + t, y + l] +
                                                            Image1_GradValue_y[x + t, y + l] * Image2_GradValue_y[x + t, y + l]) /
                                                            (Image1_GradMod[x + t, y + l] * Image2_GradMod[x + t, y + l])) > 1)
                                {
                                    w += (float)((Image1_GradMod[x + t, y + l] - Image2_GradMod[x + t, y + l]) * (Image1_GradMod[x + t, y + l] - Image2_GradMod[x + t, y + l]) /
                                                 (2 * (Image1_GradMod[x + t, y + l] * Image1_GradMod[x + t, y + l] + Image2_GradMod[x + t, y + l] * Image2_GradMod[x + t, y + l])) +
                                                 Math.Abs(Math.Acos((int)((Image1_GradValue_x[x + t, y + l] * Image2_GradValue_x[x + t, y + l] +
                                                 Image1_GradValue_y[x + t, y + l] * Image2_GradValue_y[x + t, y + l]) /
                                                 (Image1_GradMod[x + t, y + l] * Image2_GradMod[x + t, y + l]))) / (2 * Math.PI)));
                                }
                                else
                                {
                                    w += (float)((Image1_GradMod[x + t, y + l] - Image2_GradMod[x + t, y + l]) * (Image1_GradMod[x + t, y + l] - Image2_GradMod[x + t, y + l]) /
                                                 (2 * (Image1_GradMod[x + t, y + l] * Image1_GradMod[x + t, y + l] + Image2_GradMod[x + t, y + l] * Image2_GradMod[x + t, y + l])) +
                                                  Math.Abs(Math.Acos((Image1_GradValue_x[x + t, y + l] * Image2_GradValue_x[x + t, y + l] +
                                                  Image1_GradValue_y[x + t, y + l] * Image2_GradValue_y[x + t, y + l]) /
                                                  (Image1_GradMod[x + t, y + l] * Image2_GradMod[x + t, y + l])) / (2 * Math.PI)));
                                }
                                a11 += (1 + w) * (Image1_GradValue_y[x + t, y + l] + Image2_GradValue_y[x + t, y + l]) * (Image1_GradValue_y[x + t, y + l] + Image2_GradValue_y[x + t, y + l]);
                                a12_21 += -(Image1_GradValue_x[x + t, y + l] + Image2_GradValue_x[x + t, y + l]) * (Image1_GradValue_y[x + t, y + l] + Image2_GradValue_y[x + t, y + l]);
                                a22 += (1 + w) * (Image1_GradValue_x[x + t, y + l] + Image2_GradValue_x[x + t, y + l]) * (Image1_GradValue_x[x + t, y + l] + Image2_GradValue_x[x + t, y + l]);
                                b1 += (Image1_GradValue_x[x + t, y + l] + Image2_GradValue_x[x + t, y + l]) * (Image1[x + t, y + l] - Image_Modified[x + t, y + l]);
                                b2 += (Image1_GradValue_y[x + t, y + l] + Image2_GradValue_y[x + t, y + l]) * (Image1[x + t, y + l] - Image_Modified[x + t, y + l]);
                            }
                        }
                    }
                    det = a11 * a22 - a12_21 * a12_21;
                    if (det != 0)
                    {
                        a11 /= det;
                        a12_21 /= det;
                        a22 /= det;
                        VectorField_du_x[x, y] = 2 * (a11 * b1 + a12_21 * b2);
                        VectorField_du_y[x, y] = 2 * (a12_21 * b1 + a22 * b2);
                    }
                    else
                    {
                        VectorField_du_x[x, y] = 0;
                        VectorField_du_y[x, y] = 0;
                    }
                }
            }
        }

        static void Building_Final_Vector_u(GrayscaleFloatImage Final_VectorField_u, GrayscaleFloatImage VectorField_u_x, GrayscaleFloatImage VectorField_u_y,
                                            int n_gauss, double[] mask_gauss, int dir, out GrayscaleFloatImage Final_VectorField)
        {
            Final_VectorField = new GrayscaleFloatImage(Final_VectorField_u.Width, Final_VectorField_u.Height);
            GrayscaleFloatImage VectorField = new GrayscaleFloatImage(Final_VectorField_u.Width, Final_VectorField_u.Height);
            GrayscaleFloatImage New_VectorField_u = new GrayscaleFloatImage(Final_VectorField_u.Width, Final_VectorField_u.Height);
            if (dir == 1)
            {
                VectorField = VectorField_u_x;
            }
            else
            {
                VectorField = VectorField_u_y;
            }
            New_VectorField_u = Shift(Final_VectorField_u, VectorField_u_x, VectorField_u_y);
            for (int y = 0; y < Final_VectorField_u.Height; y++)
            {
                for (int x = 0; x < Final_VectorField_u.Width; x++)
                {
                    Final_VectorField[x, y] = VectorField[x, y] + New_VectorField_u[x, y];
                }
            }
        }

        static void Building_VectorField_u_Dir(GrayscaleFloatImage Final_VectorField_u_x, GrayscaleFloatImage Final_VectorField_u_y,
                                               out GrayscaleFloatImage VectorField_u_Dir, out GrayscaleFloatImage VectorField_u_Mod)
        {
            VectorField_u_Dir = new GrayscaleFloatImage(Final_VectorField_u_x.Width, Final_VectorField_u_x.Height);
            VectorField_u_Mod = new GrayscaleFloatImage(Final_VectorField_u_x.Width, Final_VectorField_u_x.Height);
            for (int y = 0; y < VectorField_u_Dir.Height; y++)
            {
                for (int x = 0; x < VectorField_u_Dir.Width; x++)
                {
                    VectorField_u_Dir[x, y] = (float)(Math.Atan2(Final_VectorField_u_y[x, y], Final_VectorField_u_x[x, y]) * 180.0 / Math.PI);
                    VectorField_u_Mod[x, y] = (float)(Math.Sqrt(Final_VectorField_u_x[x, y] * Final_VectorField_u_x[x, y] + Final_VectorField_u_y[x, y] * Final_VectorField_u_y[x, y]));
                }
            }
        }

        static float Bilinear_interpolation(GrayscaleFloatImage image, float x, float y)
        {
            int x1, x2, y1, y2;
            float Q1, Q2, Q3, Q4;
            x1 = (int)Math.Floor(x);
            x2 = x1 + 1;
            y1 = (int)Math.Ceiling(y);
            y2 = y1 - 1;
            if (x1 >= 0 && x2 >= 0 && x1 < image.Width && x2 < image.Width &&
                y1 >= 0 && y2 >= 0 && y1 < image.Height && y2 < image.Height)
            {
                Q1 = image[x1, y1];
                Q2 = image[x1, y2];
                Q3 = image[x2, y2];
                Q4 = image[x2, y1];
                return (float)(-Q1 * (x2 - x) * (y2 - y) +
                               -Q2 * (x2 - x) * (y - y1) +
                               -Q3 * (x - x1) * (y2 - y) +
                               -Q4 * (x - x1) * (y - y1));
            }
            else return -1;
        }

        static GrayscaleFloatImage Shift(GrayscaleFloatImage image, GrayscaleFloatImage VectorField_u_x, GrayscaleFloatImage VectorField_u_y)
        {
            GrayscaleFloatImage g_zv = new GrayscaleFloatImage(image.Width, image.Height);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    g_zv[x, y] = Bilinear_interpolation(image, x + VectorField_u_x[x, y], y + VectorField_u_y[x, y]);
                    if (g_zv[x, y] == -1)
                    {
                        g_zv[x, y] = image[x, y];
                    }
                }
            }
            return g_zv;
        }

        class Point_Approx : IComparable
        {
            public int[] point_position = new int[2];
            public float approx;
            public Point_Approx(int[] point_position, float approx)
            {
                this.point_position = point_position;
                this.approx = approx;
            }
            public int CompareTo(object obj)
            {
                Point_Approx p = obj as Point_Approx;
                if (p != null)
                    return this.approx.CompareTo(p.approx);
                else
                    throw new Exception("Невозможно сравнить два объекта");
            }
        }

        static void Invert_Field(GrayscaleFloatImage image, GrayscaleFloatImage VectorField_u_x, GrayscaleFloatImage VectorField_u_y,
                                                out GrayscaleFloatImage InvertField_x, out GrayscaleFloatImage InvertField_y)
        {
            InvertField_x = new GrayscaleFloatImage(image.Width, image.Height);
            InvertField_y = new GrayscaleFloatImage(image.Width, image.Height);
            float max_shift_x = 0,
                  max_shift_y = 0;
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    if (VectorField_u_x[x, y] > max_shift_x) max_shift_x = VectorField_u_x[x, y];
                    if (VectorField_u_y[x, y] > max_shift_y) max_shift_y = VectorField_u_y[x, y];
                }
            }
            max_shift_x = (float)Math.Ceiling(max_shift_x);
            max_shift_y = (float)Math.Ceiling(max_shift_y);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    SortedSet<Point_Approx> points = new SortedSet<Point_Approx>();

                    for (int i = x - (int)max_shift_x; i <= x + (int)max_shift_x; i++)
                    {
                        for (int j = y - (int)max_shift_y; j <= y + (int)max_shift_y; j++)
                        {
                            if (i >= 0 && j >= 0 && i < image.Width && j < image.Height)
                            {
                                int[] position = new int[2] { i, j };
                                float approx = (float)Math.Sqrt((i + VectorField_u_x[i, j] - x) * (i + VectorField_u_x[i, j] - x) +
                                                                (j + VectorField_u_y[i, j] - y) * (j + VectorField_u_y[i, j] - y));
                                Point_Approx point = new Point_Approx(position, approx);
                                points.Add(point);
                            }
                        }
                    }

                    int t = 0;
                    foreach (Point_Approx point in points)
                    {
                        if (t < 3)
                        {
                            int[] position = point.point_position;
                            InvertField_x[x, y] -= VectorField_u_x[position[0], position[1]];
                            InvertField_y[x, y] -= VectorField_u_y[position[0], position[1]];
                            t++;
                        }
                    }
                    if (t != 0)
                    {
                        InvertField_x[x, y] /= t;
                        InvertField_y[x, y] /= t;
                    }
                }
            }
        }

        static void Gradient(GrayscaleFloatImage image, int n, double[,] mask_x, double[,] mask_y, out GrayscaleFloatImage GradModImage,
                             out GrayscaleFloatImage GradValue_x, out GrayscaleFloatImage GradValue_y)
        {
            GradValue_x = new GrayscaleFloatImage(image.Width, image.Height);
            GradValue_y = new GrayscaleFloatImage(image.Width, image.Height);
            GradModImage = NewImage(image, 0);
            GrayscaleFloatImage image2 = NewImage(GradModImage, n);
            double sum_x = 0, sum_y = 0;
            for (int y = n; y < GradModImage.Height + n; y++)
            {
                for (int x = n; x < GradModImage.Width + n; x++)
                {
                    sum_x = 0; sum_y = 0;
                    for (int i = -n; i < n + 1; i++)
                    {
                        for (int j = -n; j < n + 1; j++)
                        {
                            sum_x = sum_x + mask_x[i + n, j + n] * image2[x + i, y + j];
                            sum_y = sum_y + mask_y[i + n, j + n] * image2[x + i, y + j];
                        }
                    }
                    GradModImage[x - n, y - n] = (float)Math.Sqrt(sum_x * sum_x + sum_y * sum_y);
                    GradValue_x[x - n, y - n] = (float)sum_x;
                    GradValue_y[x - n, y - n] = (float)sum_y;
                }
            }
        }

        static GrayscaleFloatImage Sum(GrayscaleFloatImage VectorField_1, GrayscaleFloatImage VectorField_2)
        {
            GrayscaleFloatImage SumImage = new GrayscaleFloatImage(VectorField_1.Width, VectorField_1.Height);
            for (int y = 0; y < SumImage.Height; y++)
            {
                for (int x = 0; x < SumImage.Width; x++)
                {
                    SumImage[x, y] = VectorField_1[x, y] + VectorField_2[x, y];
                }
            }
            return SumImage;
        }

        static GrayscaleFloatImage Direction(GrayscaleFloatImage grad_mod, GrayscaleFloatImage grad_dir, int step, double norm_const)
        {
            GrayscaleFloatImage image_grad = new GrayscaleFloatImage(grad_dir.Width, grad_dir.Height);
            int length = 0, x_fin = 0, y_fin = 0,
                            x_vec = 0, y_vec = 0;
            for (int x = 0; x < grad_dir.Width / step; x++)
            {
                for (int y = 0; y < grad_dir.Height / step; y++)
                {
                    length = (int)Math.Round(grad_mod[x * step, y * step] * norm_const);
                    x_fin = 0; y_fin = 0;
                    for (int r = 0; r <= length; r++)
                    {
                        x_vec = (int)Math.Round(x * step + r * Math.Cos(grad_dir[x * step, y * step]));
                        y_vec = (int)Math.Round(y * step + r * Math.Sin(grad_dir[x * step, y * step]));
                        if (x_vec >= 0 && x_vec < image_grad.Width && y_vec >= 0 && y_vec < image_grad.Height)
                        {
                            x_fin = x_vec;
                            y_fin = y_vec;
                            image_grad[x_fin, y_fin] = 255;
                            for (int i = -1; i < 2; i++)
                            {
                                for (int j = -1; j < 2; j++)
                                {
                                    if (x_fin + i >= 0 && x_fin + i < image_grad.Width && y_fin + j >= 0 && y_fin + j < image_grad.Height)
                                    {
                                        image_grad[x_fin + i, y_fin + j] = 255;
                                    }
                                }
                            }
                        }
                    }
                    for (int i = -2; i < 3; i++)
                    {
                        for (int j = -2; j < 3; j++)
                        {
                            if (x_fin + i >= 0 && x_fin + i < image_grad.Width && y_fin + j >= 0 && y_fin + j < image_grad.Height)
                            {
                                image_grad[x_fin + i, y_fin + j] = 255;
                            }
                        }
                    }
                }
            }
            return image_grad;
        }

        static void Color_Changes(GrayscaleFloatImage image1, GrayscaleFloatImage image2, GrayscaleFloatImage image2_modified,
                                  out ColorFloatImage changes_prev, out ColorFloatImage changes_new)
        {
            changes_prev = new ColorFloatImage(image1.Width, image1.Height);
            changes_new = new ColorFloatImage(image1.Width, image1.Height);
            ColorFloatPixel p;
            for (int y = 0; y < image1.Height; y++)
            {
                for (int x = 0; x < image1.Width; x++)
                {
                    p.r = image1[x, y];
                    p.g = image2[x, y];
                    p.b = 0;
                    p.a = 255;
                    changes_prev[x, y] = p;
                    p.g = image2_modified[x, y];
                    changes_new[x, y] = p;
                }
            }
        }

        static GrayscaleFloatImage Expand(GrayscaleFloatImage image, double min, double max)
        {
            GrayscaleFloatImage expanded_image = new GrayscaleFloatImage(image.Width, image.Height);
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    expanded_image[x, y] = (float)((image[x, y] - min) * 255 / (max - min));
                }
            }
            return expanded_image;
        }

        static void Difference(GrayscaleFloatImage image1, GrayscaleFloatImage image2, GrayscaleFloatImage image2_modified,
                               out GrayscaleFloatImage differ_prev, out GrayscaleFloatImage differ_new)
        {
            differ_prev = new GrayscaleFloatImage(image1.Width, image1.Height);
            differ_new = new GrayscaleFloatImage(image1.Width, image1.Height);
            double min_prev = 256, max_prev = 0, min_new = 256, max_new = 0;
            for (int y = 0; y < image1.Height; y++)
            {
                for (int x = 0; x < image1.Width; x++)
                {
                    differ_prev[x, y] = Math.Abs(image1[x, y] - image2[x, y]);
                    if (differ_prev[x, y] < min_prev) min_prev = differ_prev[x, y];
                    if (differ_prev[x, y] > max_prev) max_prev = differ_prev[x, y];
                    differ_new[x, y] = Math.Abs(image1[x, y] - image2_modified[x, y]);
                    if (differ_new[x, y] < min_new) min_new = differ_new[x, y];
                    if (differ_new[x, y] > max_new) max_new = differ_new[x, y];
                }
            }
            differ_prev = Expand(differ_prev, min_prev, max_prev);
            differ_new = Expand(differ_new, min_new, max_new);
        }

        static GrayscaleFloatImage Grid(GrayscaleFloatImage image, int step, GrayscaleFloatImage VectorField_u_x, GrayscaleFloatImage VectorField_u_y)
        {
            GrayscaleFloatImage grid = new GrayscaleFloatImage(image.Width, image.Height);
            for (int y = 0; y < image.Height / step; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    grid[x, y * step] = 255;
                }
            }
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width / step; x++)
                {
                    grid[x * step, y] = 255;
                }
            }
            return Shift(grid, VectorField_u_x, VectorField_u_y);
        }

        static void Main(string[] args)
        {
            CultureInfo c = new CultureInfo("ru");
            c.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = c;
            DateTime start;
            TimeSpan fin;
            string[] InputFiles = new string[100],
                     OutputFiles = new string[9];
            string dataDir = "../../data/",
                   resultDir = null,
                   text = null,
            file_path = "../../point_result/Weighting_symm/Weighting_symm_res.txt";
            int k;

            string[] words = new string[7],
                     tracks = new string[9],
                     tracks_res_prev = new string[9],
                     tracks_res_new = new string[9];
            double[] first_pos_x = new double[9],
                     first_pos_y = new double[9];
            double x_res, y_res, result;
            string line = null;
            StreamReader[] sr = new StreamReader[9];
            
            for (k = 0; k < 9; k++)
            {
                tracks[k] = dataDir + "track" + (k + 1).ToString() + ".txt";
                tracks_res_prev[k] = "../../test/Weighting_symm/tracks/track" + (k + 1).ToString() + "_prev_w_s.txt";
                tracks_res_new[k] = "../../test/Weighting_symm/tracks/track" + (k + 1).ToString() + "_new_w_s.txt";
                sr[k] = new StreamReader(tracks[k]);
                if ((line = sr[k].ReadLine()) != null)
                {
                    words = line.Split(new char[] { ' ' });
                    first_pos_x[k] = Convert.ToDouble(words[2]);
                    first_pos_y[k] = Convert.ToDouble(words[3]);
                }
            }
            
            for (k = 0; k < 35; k++)
            {
                InputFiles[k] = dataDir + "Im" + (k + 1).ToString() + ".bmp";
                if (!File.Exists(InputFiles[k])) return;
            }
            GrayscaleFloatImage[] Images = new GrayscaleFloatImage[100];
            for (k = 0; k < 35; k++)
            {
                Images[k] = ImageIO.FileToGrayscaleFloatImage(InputFiles[k]);
            }
            int norm_const = 1, i = 0,
                n_grad = 9, n_gauss = 6, s_gauss = 8, s_grad = 18;
            double[] mask_gauss = new double[2 * n_gauss + 1];
            double[,] mask_grad_x = new double[2 * n_grad + 1, 2 * n_grad + 1],
                      mask_grad_y = new double[2 * n_grad + 1, 2 * n_grad + 1];
            Building_Gauss_mask(n_gauss, s_gauss, out mask_gauss);
            Building_Gradient_mask(n_grad, s_grad, out mask_grad_x, out mask_grad_y);

            ColorFloatImage Color_changes_prev = new ColorFloatImage(Images[0].Width, Images[0].Height),
                            Color_changes_new = new ColorFloatImage(Images[0].Width, Images[0].Height);
            GrayscaleFloatImage Image1_GradMod = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Image2_GradMod = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Image_Modified = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Image1_GradValue_x = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Image1_GradValue_y = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Image2_GradValue_x = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Image2_GradValue_y = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                VectorField_u_Dir = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                VectorField_u_Mod = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                VectorField_u_x = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                VectorField_u_y = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                VectorField_u_x_prev = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                VectorField_u_y_prev = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                VectorField_du_x = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                VectorField_du_y = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Final_VectorField_u_x = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Final_VectorField_u_y = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Difference_prev = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Difference_new = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Grid_new = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Black = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                New_VectorField_u_x = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                New_VectorField_u_y = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Invert_VectorField_u_x = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Invert_VectorField_u_y = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Final_Invert_VectorField_u_x = new GrayscaleFloatImage(Images[0].Width, Images[0].Height),
                                Final_Invert_VectorField_u_y = new GrayscaleFloatImage(Images[0].Width, Images[0].Height);

            for (k = 35; k < 36; k++)
            {
                start = DateTime.Now;
                for (i = 0; i < k - 1; i++)
                {
                    Console.WriteLine("\ni = " + (i + 1) + "\n");
                    VectorField_u_x = Black;
                    VectorField_u_y = Black;
                    Gradient(Images[i], n_grad, mask_grad_x, mask_grad_y, out Image1_GradMod, out Image1_GradValue_x, out Image1_GradValue_y);
                    for (int j = 0; j < 100; j++)
                    {
                        Console.WriteLine("j = " + (j + 1));
                        Console.WriteLine(MSE(Images[i], Image_Modified));
                        VectorField_u_x_prev = VectorField_u_x;
                        VectorField_u_y_prev = VectorField_u_y;
                        if (j == 0)
                        {
                            Image_Modified = Images[i + 1];
                        }
                        else
                        {
                            Image_Modified = Shift(Images[i + 1], VectorField_u_x_prev, VectorField_u_y_prev);
                        }

                        Gradient(Image_Modified, n_grad, mask_grad_x, mask_grad_y, out Image2_GradMod, out Image2_GradValue_x, out Image2_GradValue_y);

                        Building_VectorField_du_W_symm(Images[i], Image_Modified, Image1_GradMod, Image1_GradValue_x, Image1_GradValue_y, Image2_GradMod, Image2_GradValue_x, Image2_GradValue_y, out VectorField_du_x, out VectorField_du_y);

                        VectorField_u_x = Gauss(Sum(VectorField_u_x_prev, Gauss(VectorField_du_x, n_gauss, mask_gauss)), n_gauss, mask_gauss);
                        VectorField_u_y = Gauss(Sum(VectorField_u_y_prev, Gauss(VectorField_du_y, n_gauss, mask_gauss)), n_gauss, mask_gauss);
                    }

                    Invert_Field(Images[i + 1], VectorField_u_x, VectorField_u_y, out Invert_VectorField_u_x, out Invert_VectorField_u_y);

                    if (i == 0)
                    {
                        Final_VectorField_u_x = VectorField_u_x;
                        Final_VectorField_u_y = VectorField_u_y;

                        Final_Invert_VectorField_u_x = Invert_VectorField_u_x;
                        Final_Invert_VectorField_u_y = Invert_VectorField_u_y;
                    }
                    else
                    {
                        Building_Final_Vector_u(Final_VectorField_u_x, VectorField_u_x, VectorField_u_y, n_gauss, mask_gauss, 1, out Final_VectorField_u_x);
                        Building_Final_Vector_u(Final_VectorField_u_y, VectorField_u_x, VectorField_u_y, n_gauss, mask_gauss, 2, out Final_VectorField_u_y);

                        Building_Final_Vector_u(Invert_VectorField_u_x, Final_Invert_VectorField_u_x, Final_Invert_VectorField_u_y, n_gauss, mask_gauss, 1, out Final_Invert_VectorField_u_x);
                        Building_Final_Vector_u(Invert_VectorField_u_y, Final_Invert_VectorField_u_x, Final_Invert_VectorField_u_y, n_gauss, mask_gauss, 2, out Final_Invert_VectorField_u_y);
                    }
                    Final_VectorField_u_x = Gauss(Final_VectorField_u_x, n_gauss, mask_gauss);
                    Final_VectorField_u_y = Gauss(Final_VectorField_u_y, n_gauss, mask_gauss);

                    Final_Invert_VectorField_u_x = Gauss(Final_Invert_VectorField_u_x, n_gauss, mask_gauss);
                    Final_Invert_VectorField_u_y = Gauss(Final_Invert_VectorField_u_y, n_gauss, mask_gauss);
                    if (i >= 0)
                    {
                        Directory.CreateDirectory("../../point_result/Weighting_symm/Im" + (i + 2).ToString() + "_Res");
                        resultDir = "../../point_result/Weighting_symm/Im" + (i + 2).ToString() + "_Res/";
                        OutputFiles[0] = resultDir + "Im" + (i + 2).ToString() + "New.png";
                        OutputFiles[1] = resultDir + "ImDirection" + (i + 2).ToString() + ".png";
                        OutputFiles[2] = resultDir + "ColorChangesPrev" + (i + 2).ToString() + ".png";
                        OutputFiles[3] = resultDir + "ColorChangesNew" + (i + 2).ToString() + ".png";
                        OutputFiles[4] = resultDir + "DifferencePrev" + (i + 2).ToString() + ".png";
                        OutputFiles[5] = resultDir + "DifferenceNew" + (i + 2).ToString() + ".png";
                        OutputFiles[6] = resultDir + "Grid" + (i + 2).ToString() + ".png";
                        OutputFiles[7] = resultDir + "Im1.png";
                        OutputFiles[8] = resultDir + "Im" + (i + 2).ToString() + ".png";
                        Image_Modified = Shift(Images[i + 1], Final_VectorField_u_x, Final_VectorField_u_y);
                        Color_Changes(Images[0], Images[i + 1], Image_Modified, out Color_changes_prev, out Color_changes_new);
                        Difference(Images[0], Images[i + 1], Image_Modified, out Difference_prev, out Difference_new);
                        Grid_new = Grid(Images[0], 16, Final_VectorField_u_x, Final_VectorField_u_y);
                        Building_VectorField_u_Dir(Final_VectorField_u_x, Final_VectorField_u_y, out VectorField_u_Dir, out VectorField_u_Mod);
                        ImageIO.ImageToFile(Image_Modified, OutputFiles[0]);
                        ImageIO.ImageToFile(Direction(VectorField_u_Mod, VectorField_u_Dir, 32, 0.5), OutputFiles[1]);
                        ImageIO.ImageToFile(Color_changes_prev, OutputFiles[2]);
                        ImageIO.ImageToFile(Color_changes_new, OutputFiles[3]);
                        ImageIO.ImageToFile(Difference_prev, OutputFiles[4]);
                        ImageIO.ImageToFile(Difference_new, OutputFiles[5]);
                        ImageIO.ImageToFile(Grid_new, OutputFiles[6]);
                        ImageIO.ImageToFile(Images[0], OutputFiles[7]);
                        ImageIO.ImageToFile(Images[i + 1], OutputFiles[8]);
                        fin = DateTime.Now - start;
                        text = "Im" + (i + 2) + " results:" + Environment.NewLine +
                                "MSE Im1 и Im" + (i + 2) + " = " + MSE(Images[0], Images[i + 1]) + Environment.NewLine +
                                "MSE Im1 и Im_Mod = " + MSE(Images[0], Image_Modified) + Environment.NewLine +
                                "MSE Im" + (i + 2) + " и Im_Mod = " + MSE(Images[i + 1], Image_Modified) + Environment.NewLine +
                                "Время выполнения = " + fin + Environment.NewLine + Environment.NewLine;
                        File.AppendAllText(file_path, text);
                        for (int q = 0; q < 9; q++)
                        {
                            if ((line = sr[q].ReadLine()) != null)
                            {
                                words = line.Split(new char[] { ' ' });
                                x_res = first_pos_x[q] - Convert.ToDouble(words[2]);
                                y_res = first_pos_y[q] - Convert.ToDouble(words[3]);
                                result = Math.Sqrt(x_res * x_res + y_res * y_res);
                                text = (i + 1) + " - " + result + Environment.NewLine;
                                File.AppendAllText(tracks_res_prev[q], text);
                                x_res -= Bilinear_interpolation(Final_Invert_VectorField_u_x, (float)Convert.ToDouble(words[2]), (float)Convert.ToDouble(words[3]));
                                y_res -= Bilinear_interpolation(Final_Invert_VectorField_u_y, (float)Convert.ToDouble(words[2]), (float)Convert.ToDouble(words[3]));
                                result = Math.Sqrt(x_res * x_res + y_res * y_res);
                                text = (i + 1) + " - " + result + Environment.NewLine;
                                File.AppendAllText(tracks_res_new[q], text);
                            }
                        }
                    }
                }
            }
            return;
        }
    }
}
